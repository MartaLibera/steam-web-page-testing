## Name
Steam Web Page Testing

## Tech Description

- **Testing library:** Java - TestNG, Selenium WebDriver 
- Explicit wait is used as principal wait type;
- WebDriver Manager used in solution;
- WebDriver Singleton pattern;
- Page Object pattern;
- Test works with Chrome browser;
- Browser starts itself up in incognito mode;
- Test data and config data is stored separetly, to work with them is recomended to use created utilitu class;

## Test Cases:

| Step | Expected result |
| ------ | ------ |
|    Navigate to main page     |        |
|      Scroll and open Privacy Policy  |    Privacy policy page is open in the new tab. <br> <br>  Switch language elements list displayed. <br> <br> Supported languages: English, Spanish, French, German, Italian, Russian, Japanese, Portuguese, Brazilian. <br><br> Policy revision signed in the current year.  |

| Step | Expected result |
| ------ | ------ |
|  Navigate to main page      |        |
|       Search "Dota 2" in the search field |   Result page is open. <br><br> Search box on result page contains searched name. <br> <br> The first name is equal to searched name.   |
|       Save information about the 1st and 2nd results from the list (name, platforms, release date, review summary result, price) |     |
|       Search the second name (received from result list) in the search field in the header      |Search box on result page contains searched name. <br> <br> Result list contains 2 stored items form the previous search. All stored data are matched.




