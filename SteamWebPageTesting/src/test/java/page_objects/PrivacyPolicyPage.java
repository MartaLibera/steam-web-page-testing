package page_objects;

import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class PrivacyPolicyPage {

    public PrivacyPolicyPage(){
    }

    public Boolean languageListVisibility() {
        WebDriverWait waitPrivacyPolicyOpened = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5));
        waitPrivacyPolicyOpened.until(ExpectedConditions.presenceOfElementLocated(By.id("languages")));

        List<WebElement> languages = DriverSingleton.driver().findElements(By.xpath("//div[@id='languages']//a[contains(@href,'privacy_agreement')]"));
        System.out.println("Number of elements:" +languages.size());

        return !languages.isEmpty();
    }

    public void selectLanguage(String chosen_language) {
        WebDriverWait waitPrivacyPolicyOpened = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5));
        waitPrivacyPolicyOpened.until(ExpectedConditions.presenceOfElementLocated(By.id("languages")));
        WebElement langsButton = DriverSingleton.driver().findElement(By.xpath("//div[@id='languages']//a[contains(@href, 'https://store.steampowered.com/privacy_agreement/" + chosen_language + "')]//img[contains(@src," + chosen_language + ")]"));
        langsButton.click();
    }

    public String signedPolicy() {

        WebElement signedPolicyText = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(3)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='newsColumn']//i[last()]")));
        String my_text = signedPolicyText.getText();
        return my_text;

    }
}
