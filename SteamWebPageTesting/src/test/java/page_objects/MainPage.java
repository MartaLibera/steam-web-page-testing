package page_objects;

import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;

public class MainPage {

    private WebElement privacyPolicyButton = DriverSingleton.driver().findElement(By.xpath("//a[contains(@href, 'https://store.steampowered.com/privacy_agreement/?snr=1_44_44_') and contains(@target, '_blank')]"));


    public MainPage() {
    }

    public void scrollBottom() {
        Actions at = new Actions(DriverSingleton.driver());
        at.sendKeys(Keys.PAGE_DOWN).build().perform();
        at.sendKeys(Keys.PAGE_DOWN).build().perform();
    }

    public void cookieAcceptMsg() {
        WebDriverWait waitCookieInfo = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5));
        waitCookieInfo.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='buttonGroup']//div[contains(@id, 'acceptAllButton')]")));

        WebElement acceptCokkiesBtn = DriverSingleton.driver().findElement(By.xpath("//div[@class='buttonGroup']//div[contains(@id, 'acceptAllButton')]"));
        acceptCokkiesBtn.click();
    }

    public void privacyPolicyOpen() {
        WebDriverWait waitPrivacyPolicyButton = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(10));
        waitPrivacyPolicyButton.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(@href, 'https://store.steampowered.com/privacy_agreement/?snr=1_44_44_') and contains(@target, '_blank')]")));
        privacyPolicyButton.click();

        ArrayList<String> wid = new ArrayList<String>(DriverSingleton.driver().getWindowHandles());
        DriverSingleton.driver().switchTo().window(wid.get(1));
    }

}

