package page_objects;
import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class SearchResultPage {

    public SearchResultPage(){

    }

    public String searchBoxText() {
        WebElement searchBoxResult = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("term")));
        String searchedText = searchBoxResult.getAttribute("value");
        return searchedText;
    }

    public String resultName(int position){
        String gamePosition = String.format("//div[@id ='search_resultsRows']//a[%d]", position);
        WebElement searchedResultName = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(gamePosition+"//span[@class ='title']")));
        String name = searchedResultName.getText();
        return name;
    }

    public String resultReleaseDate(int position){
        String gamePosition = String.format("//div[@id ='search_resultsRows']//a[%d]", position);
        WebElement searchedResultReleaseDate = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(gamePosition+"//div[contains(@class, 'search_released')]")));
        String resultReleaseDate = searchedResultReleaseDate.getText();
        return resultReleaseDate;
    }

    public String resultReview(int position){
        String gamePosition = String.format("//div[@id ='search_resultsRows']//a[%d]", position);
        WebElement searchedResultReview = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(gamePosition+"//div[contains(@class, 'search_reviewscore')]")));
        String resultReviewSummary = searchedResultReview.getAttribute("data-tooltip-html");
        return resultReviewSummary;
    }

    public String resultPrice(int position){
        String gamePosition = String.format("//div[@id ='search_resultsRows']//a[%d]", position);
        WebElement searchedResultSteamPrice = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(gamePosition+"//div[contains(@class, 'search_price')]")));
        String resultPrice = searchedResultSteamPrice.getText();
        return resultPrice;
    }

    public String resultPlatforms(int position){
        String gamePosition = String.format("//div[@id ='search_resultsRows']//a[%d]", position);
        List<WebElement> platforms = DriverSingleton.driver().findElements(By.xpath(gamePosition+"//span[contains(@class, 'platform_img')]"));

        String resultPlatforms = "";
        for (int i=0; i<platforms.size();i++){
            resultPlatforms = resultPlatforms + platforms.get(i).getAttribute("class");
        }
        return resultPlatforms;
    }

    public void secondSearch(String searched_title) {
        WebElement searchGamesField = DriverSingleton.driver().findElement(By.id("term"));
        searchGamesField.clear();
        searchGamesField.sendKeys(searched_title);
    }

    public void secondSearchClick(){
        WebElement storeSearchButton = new WebDriverWait(DriverSingleton.driver(), Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class = 'searchbar_left']//button[contains(@type, 'submit')]")));
        storeSearchButton.click();
    }

    public void searchWait(){
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

}