package page_objects;

import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage_GameSearch {

    private WebElement searchGameField = DriverSingleton.driver().findElement(By.id("store_nav_search_term"));
    private WebElement storeSearchLink = DriverSingleton.driver().findElement(By.xpath("//img[contains(@src, 'https://store.akamai.steamstatic.com/public/images/blank.gif')]"));

    public MainPage_GameSearch() {
    }

    public void inputTextSearchField(String gameTitle) {
        searchGameField.sendKeys(gameTitle);
    }

    public void searchGame() {
        storeSearchLink.click();
    }

    public String getCurrentUrl() {
        String URL = DriverSingleton.driver().getCurrentUrl();
        return URL;
    }
}

