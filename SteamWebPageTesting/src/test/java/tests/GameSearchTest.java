package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import driver.DriverSingleton;
import models.GameModel;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page_objects.MainPage_GameSearch;
import page_objects.SearchResultPage;
import utilities.ResultToJSON;
import utilities.SearchedTitleConverter;
import utilities.StartingPageConverter;

import java.io.File;


public class GameSearchTest {

    @BeforeTest
    public void SetupGameDriver(){
        StartingPageConverter testingPage = new StartingPageConverter();
        DriverSingleton.driver().get(testingPage.setDriverURL());
        DriverSingleton.driver().manage().window().maximize();
    }

    @Test
    public void GameSearchTest(){
        var mainPage = new MainPage_GameSearch();
        var resultPage = new SearchResultPage();

        SearchedTitleConverter gameTitle = new SearchedTitleConverter();
        mainPage.inputTextSearchField(gameTitle.searchedTitle());
        mainPage.searchGame();

        String actualGameUrl = mainPage.getCurrentUrl();
        String expectedGameUrlEnd = gameTitle.searchedTitle().replaceAll("\\s", "+");

        //Assertions: result page is open, searched box contains searched name, first name is equal to searched one
        Assert.assertTrue(actualGameUrl.contains(expectedGameUrlEnd), "Result page for searched game is not opened.");
        Assert.assertTrue(resultPage.searchBoxText().contains(gameTitle.searchedTitle()), "Searchbox does not contain proper value.");
        Assert.assertTrue(resultPage.resultName(1).contains(gameTitle.searchedTitle()), "Name is not equal to searched one.");

        //Step: save information about 1st and 2nd result from the list (name, platforms, release date, review summary result, price)
        ResultToJSON.writeFile(2);
        ResultToJSON.writeFile(1);

        //Assertions: search the second name - search box on result page contains searched name
        resultPage.secondSearch(resultPage.resultName(2));
        resultPage.secondSearchClick();
        resultPage.searchWait();
        Assert.assertTrue(resultPage.searchBoxText().contains(resultPage.resultName(2)), "Searchbox on result page does not contain searched name.");

        //Assertions: result list contains 2 stored items form the previous search. All stored data are matched.
        GameModel[] firstMatchListJson = null;
        GameModel[] secondMatchListJson = null;

        try {
            // json data for first match -> object
            ObjectMapper mapper = new ObjectMapper();
            firstMatchListJson = mapper.readValue(
                    new File("result1"), GameModel[].class);
            Assert.assertNotEquals(firstMatchListJson.length, 0);
            for(GameModel game : firstMatchListJson) {
                Assert.assertNotNull(game.getName(), "No Name detected for existed game.");
                Assert.assertNotEquals(game.getName(), "", "Game Name should consist at least 1 character.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for(GameModel game : firstMatchListJson) {
            Assert.assertEquals(resultPage.resultName(2), game.getName(), "Game Names are not equal.");
            Assert.assertEquals(resultPage.resultPlatforms(2), game.getPlatform1(), "Game platforms #1 are not equal.");
            Assert.assertEquals(resultPage.resultReleaseDate(2), game.getRelease(), "Game releases are not equal.");
            Assert.assertEquals(resultPage.resultReview(2), game.getReview(), "Game review summaries are not equal.");
            Assert.assertEquals(resultPage.resultPrice(2), game.getPrice(), "Game prices are not equal.");
        }

        try {
            // json data for second match -> object
            ObjectMapper mapper2 = new ObjectMapper();
            secondMatchListJson = mapper2.readValue(
                    new File("result2"), GameModel[].class);
            Assert.assertNotEquals(secondMatchListJson.length, 0);
            for(GameModel game : secondMatchListJson) {
                Assert.assertNotNull(game.getName(), "No Name detected for existed game.");
                Assert.assertNotEquals(game.getName(), "", "Game Name should consist at least 1 character.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for(GameModel game : secondMatchListJson) {
            Assert.assertEquals(resultPage.resultName(1), game.getName(), "Game Names are not equal.");
            Assert.assertEquals(resultPage.resultPlatforms(1), game.getPlatform1(), "Game platforms #1 are not equal.");
            Assert.assertEquals(resultPage.resultReleaseDate(1), game.getRelease(), "Game releases are not equal.");
            Assert.assertEquals(resultPage.resultReview(1), game.getReview(), "Game review summaries are not equal.");
            Assert.assertEquals(resultPage.resultPrice(1), game.getPrice(), "Game prices are not equal.");
        }
    }

    @AfterTest
    public void EndGameDriverSession(){
        DriverSingleton.driver().quit();
    }

}
