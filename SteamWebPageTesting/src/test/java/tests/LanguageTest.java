package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import driver.DriverSingleton;
import models.LanguagesModel;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page_objects.MainPage;
import page_objects.PrivacyPolicyPage;
import utilities.StartingPageConverter;

import java.io.File;
import java.time.Year;


public class LanguageTest {

    @BeforeTest
    public void Setup(){
        StartingPageConverter testingPage = new StartingPageConverter();
        DriverSingleton.driver().get(testingPage.setDriverURL());
        DriverSingleton.driver().manage().window().maximize();
    }

    @Test
    public void PrivacyPolicyTest(){
        var mainPage = new MainPage();
        var privPolicyPage = new PrivacyPolicyPage();

        mainPage.scrollBottom();
        mainPage.scrollBottom();
        mainPage.privacyPolicyOpen();

        Assert.assertTrue(DriverSingleton.driver().getCurrentUrl().contains("privacy_agreement"), "Privacy policy page is not opened in new tab.");
        Assert.assertTrue(privPolicyPage.languageListVisibility(), "Language choice list is not visible.");

        LanguagesModel[] myObjects = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            myObjects = mapper.readValue(new File("m.libera/src/test/resources/supported_languages_test_data.json"), LanguagesModel[].class);
            Assert.assertNotEquals(myObjects.length, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (LanguagesModel obj : myObjects ) {
            privPolicyPage.selectLanguage(obj.getName());
            String URL = DriverSingleton.driver().getCurrentUrl();
            Assert.assertTrue(URL.contains(obj.getName()));
        }

        int year = Year.now().getValue();
        String current_year = Integer.toString(year);
        Assert.assertTrue(privPolicyPage.signedPolicy().contains(current_year), "Privacy revision is not signed in current year.");

    }

    @AfterTest
    public void EndSession(){
        DriverSingleton.driver().quit();
    }

}
