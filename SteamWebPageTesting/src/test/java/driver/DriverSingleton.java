package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class DriverSingleton {
    private static WebDriver steamdriver;

    public static WebDriver driver() {
        if (steamdriver == null) {
            WebDriverManager.chromedriver.setup();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("incognito");
            options.addArguments("--lang=eng");
            steamdriver = new ChromeDriver(options);
        }
        return steamdriver;
    }
}
