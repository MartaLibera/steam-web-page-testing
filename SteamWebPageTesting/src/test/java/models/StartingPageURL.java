package models;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Comparator;
import java.util.Objects;

public class StartingPageURL implements Comparable<StartingPageURL> {
    @JsonProperty
    private String starting_page;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StartingPageURL startingPageURL = (StartingPageURL) o;
        return Objects.equals(starting_page, startingPageURL.starting_page);
    }

    @Override
    public int hashCode() {
        return Objects.hash(starting_page);
    }

    public StartingPageURL(String starting_page) {
        this.starting_page = starting_page;
    }

    public StartingPageURL() {
    }

    @Override
    public int compareTo(StartingPageURL that) {
        return Comparator
                .comparing((StartingPageURL game) -> game.starting_page)
                .compare(this, that);
    }


    public String getUrl() {
        return starting_page;
    }

    public void setUrl(String starting_page) {
        this.starting_page = starting_page;
    }
}

