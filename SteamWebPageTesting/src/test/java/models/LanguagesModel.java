package models;

import java.util.Comparator;
import java.util.Objects;

public class LanguagesModel implements Comparable<LanguagesModel> {
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LanguagesModel languagesModel = (LanguagesModel) o;
        return Objects.equals(name, languagesModel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public LanguagesModel(String name) {
        this.name = name;
    }

    public LanguagesModel() {
    }

    @Override
    public int compareTo(LanguagesModel that) {
        return Comparator
                .comparing((LanguagesModel user)->user.name)
                .compare(this, that);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}