package models;

import java.util.Comparator;
import java.util.Objects;

public class SearchedGameModel implements Comparable<SearchedGameModel> {
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchedGameModel searchedGameModel = (SearchedGameModel) o;
        return Objects.equals(name, searchedGameModel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public SearchedGameModel(String name) {
        this.name = name;
    }

    public SearchedGameModel() {
    }

    @Override
    public int compareTo(SearchedGameModel that) {
        return Comparator
                .comparing((SearchedGameModel game)->game.name)
                .compare(this, that);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}