package models;

import java.util.Comparator;
import java.util.Objects;

public class GameModel implements Comparable<GameModel> {
    private String name;
    private String platform1;
    private String release;
    private String review;
    private String price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameModel gameModel = (GameModel) o;
        return Objects.equals(platform1, gameModel.platform1) && Objects.equals(name, gameModel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, platform1, release, review, price);
    }

    public GameModel(String name, String platform1, String release, String review, String price) {
        this.name = name;
        this.platform1 = platform1;
        this.release = release;
        this.review = review;
        this.price = price;
    }

    public GameModel() {
    }

    @Override
    public int compareTo(GameModel that) {
        return Comparator
                .comparing((GameModel game)->game.name)
                .thenComparing((GameModel game)->game.platform1)
                .thenComparing((GameModel game)->game.release)
                .thenComparing((GameModel game)->game.review)
                .thenComparing((GameModel game)->game.price)
                .compare(this, that);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlatform1() {
        return platform1;
    }

    public void setPlatform1(String platform1) {
        this.platform1 = platform1;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}