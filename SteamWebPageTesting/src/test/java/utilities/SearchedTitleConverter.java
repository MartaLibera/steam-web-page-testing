package utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.SearchedGameModel;
import org.testng.Assert;

import java.io.File;

public class SearchedTitleConverter {

    public String searchedTitle() {

        SearchedGameModel[] myGames = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            myGames = mapper.readValue(new File("m.libera/src/test/resources/game_search_test_data.json"), SearchedGameModel[].class);
            Assert.assertNotEquals(myGames.length, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return myGames[0].getName();
    }

}
