package utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.StartingPageURL;
import org.testng.Assert;

import java.io.File;

public class StartingPageConverter {

    public String setDriverURL() {

        StartingPageURL[] firstUrl = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            firstUrl = mapper.readValue(
                    new File("m.libera/src/test/resources/url_input_test_data.json"), StartingPageURL[].class);
            Assert.assertNotEquals(firstUrl.length, 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return firstUrl[0].getUrl();
    }

}
