package utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import page_objects.SearchResultPage;

import java.io.FileWriter;
import java.io.IOException;

public class ResultToJSON {
    public static void writeFile(int position) {
        var resultPage = new SearchResultPage();

        String name = resultPage.resultName(position);
        String platform = resultPage.resultPlatforms(position);
        String release = resultPage.resultReleaseDate(position);
        String review = resultPage.resultReview(position);
        String price = resultPage.resultPrice(position);

        JSONArray jsonArr = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("platform1", platform);
        jsonObject.put("release", release);
        jsonObject.put("review", review);
        jsonObject.put("price", price);
        jsonArr.add(jsonObject);

        try {
            FileWriter file = new FileWriter("result"+position);
            file.write(jsonArr.toJSONString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
